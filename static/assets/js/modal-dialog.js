// Get the a tag button, that opens the modal.
var btn = document.querySelectorAll("a.modal-button");

// Get all page modals.
var modals = document.querySelectorAll('.modal');

// Get the <span> element, that closes the modal.
var spans = document.getElementsByClassName("close");

// Open the referenced modal on click.
for (var i = 0; i < btn.length; i++) {
 btn[i].onclick = function(e) {
    e.preventDefault();
    modal = document.getElementById(e.target.getAttribute("id"));
    modal.style.display = "block";
 }
}

// Close the modal on click on <span> (x).
for (var i = 0; i < spans.length; i++) {
 spans[i].onclick = function() {
    for (var index in modals) {
      if (typeof modals[index].style !== 'undefined') modals[index].style.display = "none";    
    }
 }
}

// Close the modal on click anywhere outside of the modal.
window.onclick = function(event) {
    if (event.target.classList.contains('modal')) {
     for (var index in modals) {
      if (typeof modals[index].style !== 'undefined') modals[index].style.display = "none";    
     }
    }
}
